import cv2
import numpy as np
from mDev import *
import time
mdev = mDEV()
import datetime
from thread_demo import *
from line_detection import *

def putIterationsPerSec(frame, iterations_per_sec):
    """
    Add iterations per second text to lower-left corner of a frame.
    """

    cv2.putText(frame, "{:.0f} iterations/sec".format(iterations_per_sec),
        (10, 450), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 255, 255))
    return frame

def noThreading(source=0):
    """Grab and show video frames without multithreading."""

    cap = cv2.VideoCapture(source)
    cps = CountsPerSec().start()

    while True:
        (grabbed, frame) = cap.read()
        if not grabbed or cv2.waitKey(1) == ord("q"):
            break
        
        
        frame = putIterationsPerSec(frame, cps.countsPerSec())
        cv2.imshow("Video", frame)
        cps.increment()
        




def threadindVideoGet(source=0):
    """
    Dedicated thread for grabbing video frames with VideoGet object.
    Main thread shows video frames.
    """
    
    stop_sign = cv2.CascadeClassifier('cascade_stop_sign.xml')
    pieton_sign = cv2.CascadeClassifier('pieton.xml')
    # cap = cv2.VideoCapture("/dev/video0") # check this
    vitesse = 350
    angle = 90
    variable=0
    mdev.setServo('3', 90)
    mdev.setServo('2', 80)
    
    video_getter = VideoGet(source).start()
    # cps = CountsPerSec().start()
    
    while True:
        if (cv2.waitKey(1) == ord("q")) or video_getter.stopped:
            video_getter.stop()
            cv2.destroyAllWindows()
            mdev.move(0, 0)
            break
        
        frame = video_getter.frame
        # convert the frame to grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        mdev.move(vitesse,vitesse, angle)
        canny_output = canny(img=frame)
        masked_output = region_of_intrest(img=canny_output)
        masked_output2 = region_of_intrest(img=gray) 
        lines = houghLines(masked_output)
        
        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line.reshape(4)
                
                detectedAngle = (np.arctan2(y2 - y1, x2 - x1) * 180 / np.pi) 
                #print(angle)

                cv.line(frame, (x1, y1), (x2,y2-30), (255, 0, 0), 5)
                if detectedAngle > 0:
                    x = 90 - detectedAngle
                    angle = int(x) + 90
                    print(angle)
                    mdev.move(vitesse,vitesse, angle)
                    #mdev.setServo('2', angle)
                    
                if detectedAngle < 0:
                    x = int(detectedAngle) + 90
                    angle = 90-x
                    print(angle)
                    mdev.move(vitesse,vitesse, angle)
                    #mdev.setServo('2', angle)
        line_image = display_lines(frame, lines)                
        cv2.imshow("Video", masked_output2)
        # cps.increment()
        
threadindVideoGet()