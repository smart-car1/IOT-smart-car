import cv2

# Stop Sign Cascade Classifier xml
no_entry = cv2.CascadeClassifier('C:/Users/ramyn/Downloads/stop_sign_detection-main/stop_sign_detection-main/noentry.xml')
cap = cv2.VideoCapture(0)

while cap.isOpened():
    _, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    no_entry_scaled = no_entry.detectMultiScale(gray, 1.3, 5)

    # Detect the stop sign, x,y = origin points, w = width, h = height
    for (x, y, w, h) in no_entry_scaled:
        # Draw rectangle around the stop sign
        no_entry_rectangle = cv2.rectangle(img, (x,y),
                                            (x+w, y+h),
                                            (0, 255, 0), 3)
        # Write "Stop sign" on the bottom of the rectangle
        no_entry_text = cv2.putText(img=no_entry_rectangle,
                                     text="No entry",
                                     org=(x, y+h+30),
                                     fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                     fontScale=1, color=(0, 0, 255),
                                     thickness=2, lineType=cv2.LINE_4)
    cv2.imshow("img", img)
    key = cv2.waitKey(30)
    if key == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break
