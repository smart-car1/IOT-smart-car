
from datetime import datetime
import cv2 as cv
from threading import Thread
from line_detection import *

class CountsPerSec:
    """
    Class that tracks the numv=ber of occurences ("counts") of an event
    and returns the frequency in occurences (counts) per second (secs).
    """
    
    def __init__(self):
        self._start_time = None
        self._num_occurrences = 0
        
    
    def start(self):
        self._start_time = datetime.now()
        return self
    
    def increment(self):
        self._num_occurrences += 1
        
    
    def countsPerSec(self):
        elapsed_time = (datetime.now() - self._start_time).total_seconds()
        return self._num_occurrences / elapsed_time
    

class VideoGet:
    """
    Class that continuously gets frames from a VideoCapture object with
    a dedicated thread.
    """
    
    def __init__(self, src=0):
        self.stream = cv.VideoCapture(src)
        (self.grabbed, self.frame) = self.stream.read()
        self.stopped = False
        
    def start(self):
        Thread(target=self.get, args=()).start()
        return self
    
    def get(self):
        while not self.stopped:
            if not self.grabbed:
                self.stop()
            else:
                (self.grabbed, self.frame) = self.stream.read()
                
    
    def stop(self):
        self.stopped = True
        
