import cv2 as cv 
import numpy as np

def canny(img):
    """ Function that returns a canny image 
    Parameters:
        img (np.array): Input image
    """
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    kernel = 5
    blur = cv.GaussianBlur(gray, (kernel, kernel), 0)
    canny = cv.Canny(blur, 150, 250)
    return canny

def region_of_intrest(img):
    """ Function that returns a masked image 
    Parameters:
        img (np.array): Input image
    """
    height = img.shape[0]
    width = img.shape[1]

    mask = np.zeros_like(img)
    triangle = np.array([[((width//2)-120, height-60), ((width//2)-120, 200), ((width//2)+120, 200), ((width//2)+120, height-60)]], np.int32)
    cv.fillPoly(mask, triangle, 255)
    masked_image = cv.bitwise_and(img, mask)
    return masked_image

def houghLines(img):
    """ function that removes the noise, it will read the images and will try to find a patern  
    Parameters:
        img (np.array): Input image
    """
    houghLines = cv.HoughLinesP(img, 2, np.pi/180, 100, np.array([]), minLineLength=50, maxLineGap=10)
    return houghLines
    
def display_lines(img, lines):
    """function that puts the points given by the lines parameter on the image
    Parameters:
        img (np.array): Input image
        lines: array 
    """
    
    # line_image = np.zeros_like(img)
    if lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            
            # angle = np.arctan2(y2 - y1, x2 - x1) * 180 / np.pi
            # print("Angle: {:.2f}".format(angle))

            cv.line(img, (x1, y1), (x2,y2-30), (255, 0, 0), 5)
    return img


# canny_output = canny(img=frame)
# masked_output = region_of_intrest(img=canny_output)
# lines = houghLines(masked_output)
# line_image = display_lines(frame, lines)