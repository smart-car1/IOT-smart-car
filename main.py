import numpy as np
import cv2
from mDev import *
import time
mdev = mDEV()
import datetime
from line_detection import *
from thread_demo import VideoGet


stop_sign = cv2.CascadeClassifier('cascade_stop_sign.xml')
pieton_sign = cv2.CascadeClassifier('pieton.xml')
cap = cv2.VideoCapture("/dev/video0") # check this
vitesse = 500
angle = 90
variable=0
mdev.setServo('3', 90)
mdev.setServo('2', 90)

video_getter = VideoGet().start()
while cap.isOpened():
    # capture a frame from the camera
    frame = video_getter.frame

    # convert the frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    mdev.move(vitesse,vitesse, angle)
    canny_output = canny(img=frame)
    masked_output = region_of_intrest(img=canny_output)
    masked_output2 = region_of_intrest(img=gray) 
    lines = houghLines(masked_output)
    #line_image = display_lines(frame, lines)
    
    if lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            
            detectedAngle = (np.arctan2(y2 - y1, x2 - x1) * 180 / np.pi) 
            #print(angle)

            cv.line(frame, (x1, y1), (x2,y2-30), (255, 0, 0), 5)
            if detectedAngle > 0:
                x = 90 - detectedAngle
                angle = int(x) + 90
                print(angle)
                mdev.move(vitesse,vitesse, angle)
                #mdev.setServo('2', angle)
                
            if detectedAngle < 0:
                x = int(detectedAngle) + 90
                angle = 90-x
                print(angle)
                mdev.move(vitesse,vitesse, angle)
                #mdev.setServo('2', angle)
            
    # Detect the stop sign, x,y = q points, w = width, h = height
    stop_sign_scaled = stop_sign.detectMultiScale(gray, 1.3, 5)
    pieton_sign_scaled = pieton_sign.detectMultiScale(gray, 1.3, 5)
    
    if len(stop_sign_scaled) > 0 :
        for (x, y, w, h) in stop_sign_scaled:
            # Draw rectangle around the stop sign
            sign_width = w
            sign_height = h
            stop_sign_rectangle = cv2.rectangle(frame, (x,y),
                                                (x+w, y+h),
                                                (0, 255, 0), 3)
        
            # Write "Stop sign" on the bottom of the rectangle
            stop_sign_text = cv2.putText(img=stop_sign_rectangle,
                                         text="Stop Sign",
                                         org=(x, y+h+30),
                                         fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                         fontScale=1, color=(0, 0, 255),
                                         thickness=2, lineType=cv2.LINE_4)
        if (sign_height > 120 or sign_width > 120) and variable == 0:
            #time.sleep(1)
            vitesse = 0
            mdev.move(vitesse, vitesse, angle)
            time.sleep(2)
            vitesse = 380
            mdev.move(vitesse, vitesse, angle)
            variable += 1
            
    elif len(pieton_sign_scaled) > 0:
        for (x, y, w, h) in pieton_sign_scaled:
            # Draw rectangle around the stop sign
            sign_width = w
            sign_height = h
            pieton_sign_rectangle = cv2.rectangle(frame, (x,y),
                                                (x+w, y+h),
                                                (0, 255, 0), 3)
        
            # Write "Stop sign" on the bottom of the rectangle
            pieton_sign_text = cv2.putText(img=pieton_sign_rectangle,
                                         text="Pieton Sign",
                                         org=(x, y+h+30),
                                         fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                         fontScale=1, color=(0, 0, 255),
                                         thickness=2, lineType=cv2.LINE_4)
        if (sign_height > 0 or sign_width > 0) and variable == 0:
            #time.sleep(1)
            vitesse = 300
            mdev.move(vitesse, vitesse, angle)
            variable += 1

    else:
        mdev.move(vitesse,vitesse, angle)      
        
                    
                    


    # show the frame with the lines
    cv2.imshow("Camera", frame)
    cv2.imshow("mask", masked_output2)
        

    # break the loop if the 'q' key is pressed
    key = cv2.waitKey(30)
    if key == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        mdev.move(0, 0)
        break

